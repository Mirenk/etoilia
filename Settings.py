import os
import datetime
import pathlib
import configparser
from CSVUtil import CSVUtil
from ICReaderUtil import FakeICReader, RealICReader
from SubjectUtil import Subject, SubjectList
from StudentUtil import StudentCSV

class Settings:
    config = configparser.ConfigParser()
    configpath = "settings.ini"

    def __init__(self, teacherid):
        self.teacherid = teacherid
        # デフォルト設定(readSettings()で書き換わるもの)
        self.subjectcsvpass = ""
        self.studentdir = ""
        self.studentprefix = ""
        self.hasexam = False
        self.startexamdate = None
        self.endexamdate = None
        self.exceptcsv = ""
        self.usefakereader = True
        # フィールド初期化部分
        self.fakereadercsv = ""
        
        # ファイルが見つからない場合、初期設定を行い設定ファイルを出力する
        if not self.readSettings(self.teacherid):
            self.setupFiles()
            self.writeSettings("DEFAULT")
        # 疑似リーダフラグが立っている場合、CSVファイルパスを設定
        elif self.usefakereader:
            self.setUseFakeReader(False)

        # SubjectList初期化
        self.reloadSubjectList()
        self.reloadExam()
        # ExceptDayロード
        self.reloadExceptCSV()

    # 初期設定
    def setupFiles(self):
        print("I: 初期設定を行います。")
        self.setSubjectCSV(False)
        self.setStudentCSVprefix()
        self.setExamTerm(reload=False)
        self.setExceptCSV(False)
        self.setUseFakeReader()

    # 各CSVを設定するメソッド
    def setCSV(self, msg, header, csvval):
        while(True):
            print(msg + "\n取り消すにはqを入力してください。")
            print("現在の設定: " + csvval)
            csvpath = input()
            if csvpath == "q":
                print("取り消しました。")
                csvpath = csvval
            
            if not os.path.exists(csvpath):
                print("E: ファイルが見つかりません。")
                continue
            elif CSVUtil.readHeader(csvpath) != header:
                print("E: ファイル形式が不正です。以下のヘッダを持つCSVファイルを設定してください")
                print(header)
                continue
            else:
                csvval = csvpath
                print("I: " + csvval + "に設定しました。")
                break
        return csvval

    # 講義データ設定メソッド
    def setSubjectCSV(self, reload=True):
        msg = "講義データのファイルパスを入力してください。"
        header = ["講義ID", "科目名", "ID", "教員名", "開始時間", "終了時間", "出席限度(分)", "遅刻限度(分)", "試験", "履修者数"]
        self.subjectcsvpass = self.setCSV(msg, header, self.subjectcsvpass)
        if reload:
            self.reloadSubjectList()

    # 考査期間設定メソッド
    def setExamTerm(self, startdate=None, enddate=None, reload=True):
        if startdate == None and enddate == None:
            print("考査期間を登録しますか？(y/n)")
            while True:
                check = input()

                if check == "y":
                    break
                elif check == "n":
                    self.hasexam = False
                    print("I: 考査期間を取り消しました。")
                    return
                else:
                    print("E: 入力値が不正です。yかnで入力してください。")
                    continue

        if startdate == None:
            print("考査期間開始日を入力してください(年(西暦)-月-日)")
            startdate = input()
        while True:
            try:
                self.startexamdate = datetime.datetime.strptime(startdate, "%Y-%m-%d")
                break
            except ValueError:
                print("E:入力値が不正です。年(西暦)-月-日の形式で入力してください。")
                startdate = input()
                continue
        print("I: 考査期間開始日を" + startdate + "に設定しました。")

        if enddate == None:
            print("考査期間終了日を入力してください(年(西暦)-月-日)")
            enddate = input()
        while True:
            try:
                self.endexamdate = datetime.datetime.strptime(enddate, "%Y-%m-%d")
                break
            except ValueError:
                print("E:入力値が不正です。年(西暦)-月-日の形式で入力してください。")
                enddate = input()
                continue
        print("I: 考査期間終了日を" + enddate + "に設定しました。")
        self.hasexam = True
        if reload:
            self.reloadExam()

    # 特別日程設定メソッド
    def setExceptCSV(self, reload=True):
        msg = "特別日程データのファイルパスを入力してください。"
        header = ["変更対象講義ID", "実施日付", "開始時刻", "終了時刻"]
        self.exceptcsv = self.setCSV(msg, header, self.exceptcsv)
        if reload:
            self.reloadExceptCSV()

    # 疑似カードリーダ設定メソッド
    def setUseFakeReader(self, menu=True):
        if menu:
            print("疑似カードリーダ機能を有効化しますか？(y/n)")
            print("カードリーダを使用せず出欠データで出席判定を行いたい場合、有効化してください。")
        else:
            check = "y"
        while(True):
            if menu:
                check = input()
            if check == "y":
                msg = "出欠データのファイルパスを入力してください。"
                header = ["年月日", "時刻", "IDm"]
                self.fakereadercsv = self.setCSV(msg, header, self.fakereadercsv)
                break
            elif check == "n":
                self.fakereadercsv = ""
                break
            else:
                print("E: 入力値が不正です。yかnで入力してください。")
                continue
        self.setICReader()

    # 疑似カードリーダフラグ設定メソッド
    def setICReader(self):
        if self.fakereadercsv == "":
            self.icreader = RealICReader()
            self.usefakereader = False
            print("I: 疑似カードリーダ機能を無効化しました。")
            return
        else:
            self.icreader = FakeICReader(self.fakereadercsv)
            self.usefakereader = True
            print("I: 疑似カードリーダ機能を有効化しました。")
            return

    # 履修者データ設定メソッド
    def setStudentCSVprefix(self):
        while(True):
            print("各履修者データが存在するディレクトリのパスを入力してください。\n取り消すにはqを入力してください。")
            print("現在の設定: " + self.studentdir)
            dirpath = input()

            if dirpath == "q":
                print("I: 取り消しました。")
                dirpath = self.studentdir

            if not os.path.exists(dirpath):
                print("E: ディレクトリが見つかりません。")
                continue
            else:
                self.studentdir = dirpath
                print("I: " + self.studentdir + "に設定しました。")
                break

        while(True):
            print("ファイルの接頭辞を入力してください。\n取り消すにはqを入力してください。")
            print("現在の設定: " + self.studentprefix)
            prefix = input()
            
            if prefix == "q":
                print("I: 取り消しました。")
                prefix = self.studentprefix
            
            filelist = list(pathlib.Path(dirpath).glob(prefix + "[M,T,W,F]*[1-5]*.csv"))

            if len(filelist) == 0:
                print("E: 該当するファイルが見つかりません。")
                continue
            else:
                self.studentprefix = prefix
                print("I: " + self.studentprefix + "に設定しました。")
                return

    # 講義データ再読み込みメソッド
    def reloadSubjectList(self):
        if not os.path.exists(self.subjectcsvpass):
            print("E: 講義データが見つかりません。設定を行ってください。")
            self.setSubjectCSV()
        self.subjectlist = SubjectList(self.subjectcsvpass)

    # 履修者リスト取得メソッド
    def getStudentList(self, subjectid):
        csvpass = os.path.join(self.studentdir, self.studentprefix + subjectid + ".csv")
        if not os.path.exists(csvpass):
            print("E: 履修者データが見つかりません。設定を行ってください。")
            self.setStudentCSVprefix()
        return CSVUtil(csvpass).getClassList(StudentCSV)

    # 考査期間再読み込みメソッド
    def reloadExam(self):
        self.subjectlist.setExamTeam(self.hasexam, self.startexamdate, self.endexamdate)

    # 特別日程再読み込みメソッド
    def reloadExceptCSV(self):
        self.subjectlist.setExceptDayFromFile(self.exceptcsv)

    # 設定ファイル読込
    def readSettings(self, section):
        if not os.path.exists(Settings.configpath):
            return False
        
        # ファイル読込
        Settings.config.read(Settings.configpath, encoding="utf-8")

        if not Settings.config.has_section(section):
            section = "DEFAULT"

        # ファイルから各値をセット
        self.subjectcsvpass = Settings.config.get(section, "subjectcsvpass")
        self.studentdir = Settings.config.get(section, "studentdir")
        self.studentprefix = Settings.config.get(section, "studentprefix")
        self.hasexam = Settings.config.getboolean(section, "hasexam")
        startexamdate = Settings.config.get(section, "startexamdate")
        endexamdate = Settings.config.get(section, "endexamdate")
        self.exceptcsv = Settings.config.get(section, "exceptcsv")
        self.usefakereader = Settings.config.getboolean(section, "usefakereader")

        self.setExamTerm(startexamdate, endexamdate, False)

        return True
        

    # 設定ファイル書き出し
    def writeSettings(self, section):
        # セクション追加
        if not Settings.config.has_section(section) and section != "DEFAULT":
            Settings.config.add_section(section)

        # 現在の各値をセット
        Settings.config.set(section, "subjectcsvpass", self.subjectcsvpass)
        Settings.config.set(section, "studentdir", self.studentdir)
        Settings.config.set(section, "studentprefix", self.studentprefix)
        Settings.config.set(section, "hasexam", str(self.hasexam))
        Settings.config.set(section, "startexamdate", self.startexamdate.strftime("%Y-%m-%d"))
        Settings.config.set(section, "endexamdate", self.endexamdate.strftime("%Y-%m-%d"))
        Settings.config.set(section, "exceptcsv", self.exceptcsv)
        Settings.config.set(section, "usefakereader", str(self.usefakereader))

        with open(Settings.configpath, 'w', encoding="utf-8", newline="") as file:
            Settings.config.write(file)

    # 設定関数
    def settingMenu(self):
        while(True):
            print("設定項目を選んでください。\n")
            print("1:講義データ設定")
            print("2:履修者データ設定")
            print("3:考査期間設定")
            print("4:特別日程データ設定")
            print("5:疑似カードリーダ(出欠データ)設定")
            print("r:設定ファイルより設定読み込み")
            print("w:設定ファイルに設定書き込み")
            print("q:メインメニューに戻る")

            menu = input()

            if menu == "1":
                self.setSubjectCSV()
            elif menu == "2":
                self.setStudentCSVprefix()
            elif menu == "3":
                self.setExamTerm()
            elif menu == "4":
                self.setExceptCSV()
            elif menu == "5":
                self.setUseFakeReader()
            elif menu == "r":
                self.readSettings(self.teacherid)
            elif menu == "w":
                self.writeSettings(self.teacherid)
            elif menu == "q":
                return
            else:
                print("E: 入力値が不正です。もう一度やり直してください。")