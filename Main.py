# 標準ライブラリ
import os
import datetime
# 自作ライブラリ
from SubjectUtil import Subject, SubjectList
from StudentUtil import StudentCSV
from CSVUtil import CSVUtil

# 設定モジュール
from Settings import Settings
# 出席モジュール
import Attendance

# 自動判別フラグ
autodetect = True
# 自動判別時間差
minute = 60

# 時刻(今回は手入力で自動判別を確認)
#now = datetime.datetime.now()
while True:
    nowstr = input("自動判別に利用する日時を入力(%Y-%m-%d %H:%M)")
    try:
        now = datetime.datetime.strptime(nowstr,"%Y-%m-%d %H:%M")
        break
    except ValueError:
        print("E: 入力値が不正です")
        continue

def initialize():
    teacherid = input("教師IDを入力してください:")

    # 設定オブジェクト生成
    settings = Settings(teacherid)

    # 教科判別
    subject = Attendance.detectSubject(teacherid, now, minute, settings, settings.usefakereader, autodetect)
    
    # 教科フォルダを生成
    subjectdir = "data/" + teacherid + "/" + subject.getSubjectId() + "_" + subject.getSubjectName()

    if not os.path.exists(subjectdir):
        os.makedirs(subjectdir)

    return settings, subject
    

def mainMenu(settings, subject):
    attendlist = []
    while(True):
        print("実行項目を選んでください。\n")
        print("1:出席受付")
        print("2:出席書き出し")
        print("3:設定")
        print("q:プログラムを終了")

        menu = input()

        if menu == "1":
            attendlist = Attendance.checkAttendance(subject, settings.icreader)
            Attendance.writeTempAttendance(attendlist, subject, settings.icreader.getDate())
            continue
        elif menu == "2":
            Attendance.writeAttendance(subject)
            continue
        elif menu == "3":
            settings.settingMenu()
            continue
        elif menu == "q":
            exit()
        else:
            print("E: 入力値が不正です。もう一度やり直してください。")


if __name__ == "__main__":
    settings, subject = initialize()

    mainMenu(settings, subject)

