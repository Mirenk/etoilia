import datetime
import os
import pathlib
import Settings
from CSVUtil import CSVUtil
from ICReaderUtil import ICReaderInterrupt
from SubjectUtil import SubjectList
from StudentUtil import StudentAttendance

def detectSubject(teacherid, now, minute, settings, fakereader, autodetect=False):
    # 教師IDに該当する講義リスト
    subject = None
    subjectlist = settings.subjectlist
    detectsubject, subjects, exams = subjectlist.getSubjectFromTeacherId(teacherid, now, minute)

    if len(subjects) == 0:
        print("E: 教師IDが不正です。プログラムを終了します。")
        exit()

    # 疑似カードリーダを使用しない場合または自動判別がオンの場合、自動判別
    if (not fakereader) or autodetect:
        if len(detectsubject) > 1:
            print("")
            print("I: 該当講義が複数見つかりました。")
            print("")
            for tmpsubject in detectsubject:
                index = detectsubject.index(tmpsubject)
                print(str(index) + ":" + tmpsubject.getSubjectId() + " " + tmpsubject.getSubjectName())
            print("")
            print("単体で出席受付を行う場合はその講義の番号を入れてください。")
            print("同時に出席受付を行う場合はそれらの講義の番号をコンマ区切り(,)で入力してください。")
            print("入力された番号の先頭の講義に出席記録を行います。")
            print("対象講義が存在しない場合はnを入力してください。")
            while True:
                index = input()

                if index == "n":
                    break # 入力された教師IDを持つ講義の手動選択に進む

                index = index.split(",")

                try:
                    subject = detectsubject[int(index[0])]
                    subjectid = subject.getSubjectId()
                    subject.addStudentList(settings.getStudentList(subjectid))

                    for i in index[1:]:
                        tmpsubject = detectsubject[int(i)]
                        subjectid = tmpsubject.getSubjectId()
                        subject.addSubjectId(subjectid)
                        subject.addStudentList(settings.getStudentList(subjectid))
                    break
                except Exception as e:
                    print("E: 入力が不正です。やり直してください。")
                    print(e)
                    continue
        elif len(detectsubject) == 1:
            print(detectsubject[0].getSubjectName() + "でよろしいですか？(y/n)")
            while True:
                check = input()

                if check == "y":
                    subject = detectsubject[0]
                    subjectid = subject.getSubjectId()
                    subject.addStudentList(settings.getStudentList(subjectid))
                    break
                elif check == "n":
                    break
                else:
                    print("E: yかnで入力してください。")
        
    # 自動判別されなかった場合、教員IDに対応する講義リストから手動選択
    if subject == None:
        if len(exams) != 0:
            print("考査用出席受付でよろしいですか？(y/n)")
            while True:
                check = input()

                if check == "y":
                    subjects = exams
                    break
                elif check == "n":
                    break
                else:
                    print("E: 入力が不正です。yかnで入力してください。")
                    continue
        
        print("")
        print("出席受付を行いたい講義の番号を選択してください。")
        print("複数の講義の出席受付を行いたい場合、コンマ区切り(,)で入力してください。")
        print("入力された番号の先頭の講義に出席記録を行います。")
        print("")
        for tmpsubject in subjects:
            index = subjects.index(tmpsubject)
            print(str(index) + ":" + tmpsubject.getSubjectId() + " "  + tmpsubject.getSubjectName())

        while True:
            index = input().split(",")

            try:
                subject = detectsubject[int(index[0])]
                subjectid = subject.getSubjectId()
                subject.addStudentList(settings.getStudentList(subjectid))

                for i in index[1:]:
                    tmpsubject = detectsubject[int(i)]
                    subjectid = tmpsubject.getSubjectId()
                    subject.addSubjectId(subjectid)
                    subject.addStudentList(settings.getStudentList(subjectid))
                break
            except Exception as e:
                print("E: 入力が不正です。やり直してください。")
                print(e)
                continue
        
    return subject

def checkAttendance(subject, icreader):
    studentlist = subject.getStudentList()
    # 講義の時間定義部分切り出し
    timelist = subject.getTimeList()

    # 出席状況リスト作成(初期状態は全員欠席)
    attendlist = []
    for student in studentlist:
        studentattend = StudentAttendance.fromStudent(student, timelist)
        attendlist.append(studentattend)

    # 出席状況登録
    while(True):
        try:
            readdata = icreader.getReadData()
        except ICReaderInterrupt:
            break

        for student in studentlist:
            if student.isMyIdm(readdata.getIdm()):
                # 該当生徒のインデックスを調べ、その生徒の出席状況を記録する
                index = studentlist.index(student)
                attendlist[index].setAttendance(readdata.getTime())
                break
        else:
            print("履修者ではありません。")

    return attendlist

# 教員フォルダに当日の出席リストを出力する関数
def writeTempAttendance(attendlist, subject, date, writetimelist=True):
    # 出力CSVリスト
    header = ["学籍番号", "氏名", "フリガナ", "性別", "出席時刻", "出席状況"]
    if writetimelist:
        header.extend(["", "講義開始時刻", "出席限度", "欠席限度"])

    firststudent = attendlist[0].getList(gettimelist=writetimelist)
    attendlistcsv = [header, firststudent,]
    for attend in attendlist[1:]:
        attendlistcsv.append(attend.getList(padding=writetimelist))

    filepath = "data/" + subject.getTeacherId() + "/" + subject.getSubjectId() + "_" + subject.getSubjectName() + "/" +  date + "-attend.csv"

    # 書き出し
    while True:
        try:
            CSVUtil.write(attendlistcsv, filepath)
            print("I: " + filepath + "に書き出しました。")
            break
        except PermissionError:
            print("E: 当日出席ファイル(" + filepath + ")が書き出せません。")
            print("開いているソフトウェアがないかどうか、権限があるかどうかを確認してください。")
            print("続行するにはEnter、キャンセルするにはqを入力してください(出席状況は失われます)")
            menu = input("")

            if menu == "q":
                print("書き出しを中断しました。")
                break
            else:
                continue

# 今までの出席状況をまとめて書き出す関数
def writeAttendance(subject):
    studentlist = subject.getStudentList()

    # dataフォルダに保存された出席状況ファイルを正規表現で取得
    attendfilepath = "data/" + subject.getTeacherId() + "/" + subject.getSubjectId() + "_" + subject.getSubjectName()
    attendfiles = list(pathlib.Path(attendfilepath).glob("[1-2][0-9][0-9][0-9]-[0-1][0-9]-[0-3][0-9]-attend.csv")) 
    if len(attendfiles) == 0:
        print("E: 過去の出席状況データが見つかりません。")
        return

    # 学生部分のヘッダ
    header = ["学籍番号", "氏名", "フリガナ", "性別"]
    attendlist = []

    for student in studentlist:
        index = studentlist.index(student)
        templist = student.getList()
        for attendfile in attendfiles:
            # 処理はじめのみヘッダに日付(ファイル名から取得)を追加
            if index == 0:
                header.append(attendfile.name[:10])
            # CSVから6列目を読みこみ、生徒の行に追加
            csv = CSVUtil.read(attendfile)
            templist.append(csv[index][5])
        attendlist.append(templist)

    attendcsv = [header, ]
    attendcsv.extend(attendlist)

    filename = input("ファイル名を入力してください: ")

    if os.path.exists(filename):
        while(True):
            check = input("既存ファイルを上書きしてもよろしいですか？(y/n)")

            if check == "y":
                break
            elif check == "n":
                print("I: キャンセルしました。")
                return
            else:
                print("E: yかnで入力してください。")
                continue

    while(True):
        try:
            CSVUtil.write(attendcsv, filename)
            print("I: "+ filename + "に書き出しました。")
            break
        except PermissionError:
            print("E: 出席ファイル(" + filename + ")が書き出せません。")
            print("開いているソフトウェアがないかどうか、権限があるかどうかを確認してください。")
            print("続行するにはEnter、名前を変更するにはr, キャンセルするにはqを入力してください")
            menu = input("")

            if menu == "q":
                print("I: 書き出しを中断しました。")
                break
            elif menu == "r":
                filename = input("ファイル名を再入力してください。")
                continue
            else:
                continue

    return