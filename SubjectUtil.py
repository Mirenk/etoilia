import copy
import datetime
from CSVUtil import CSVUtil

#
# 例外
#
class NoExamTimeException(Exception):
    pass

#
# Subjectクラス
#
class Subject:
    def __init__(self, list):
        self.__subjectid = list[0]
        self.__subjectname = list[1]
        self.__teacherid = list[2]
        self.__teachername = list[3]
        self.__starttime = list[4]
        self.__endtime = list[5]
        self.__attendborder = list[6]
        self.__lateborder = list[7]
        if list[8] == "あり":
            self.__hasexam = True
        else:
            self.__hasexam = False
        self.__studentnum = list[9]

        self.__isexam = False
        self.__studentlist = []

    def setExam(self, examflag):
        if self.__hasexam:
            self.__isexam = examflag

    def setTime(self, starttime, endtime):
        self.__starttime = starttime
        self.__endtime = endtime

    def setBorder(self, attendborder, lateborder):
        self.__attendborder = attendborder
        self.__lateborder = lateborder

    def addStudentList(self, list):
        self.__studentlist.extend(list)

    def addSubjectId(self, id):
        self.__subjectid = self.__subjectid + "-" + id

    def getSubjectId(self):
        return self.__subjectid

    def getSubjectName(self):
        return self.__subjectname

    def getTeacherId(self):
        return self.__teacherid

    def getTimeList(self):
        return [self.__starttime, self.__attendborder, self.__lateborder]

    def getStudentList(self):
        return self.__studentlist

    def hasExam(self):
        return self.__hasexam

    def isExam(self):
        return self.__isexam

    def isThisSubjectId(self, subjectid):
        if(subjectid == self.__subjectid):
            return True
        return False
    
    def isThisSubjectTeacher(self, teacherid):
        if(teacherid == self.__teacherid):
            return True
        return False

    def isThisSubjectWeekDay(self, weekday):
        weekdayid = ["M", "T", "W", "Th", "F", "", ""]
        weekday = weekdayid[weekday]
        if(weekday in self.__subjectid):
            return True
        return False

    def isNearTime(self, nowtime, minute):
        time = datetime.datetime.strptime(self.__starttime, "%H:%M")
        neartime = time - datetime.timedelta(minutes=minute)

        if (nowtime > neartime.time()) and (nowtime < time.time()):
            return True
        return False

#
# 特別日程クラス
# 
class ExceptDay:
    def __init__(self, list):
        self.__subjectid = list[0]
        self.__date = datetime.datetime.strptime(list[1], "%Y-%m-%d").date()
        self.__starttime = list[2]
        self.__endtime = list[3]

    def isExceptDay(self, now):
        if now.date() == self.__date:
            return True
        return False

    def isThisSubjectId(self, subjectid):
        if subjectid == self.__subjectid:
            return True
        return False

    def getSubjectId(self):
        return self.__subjectid

    def getTime(self):
        return self.__starttime, self.__endtime

#
# Subjectクラスのリストクラス
#
class SubjectList:
    def __init__(self, subjectcsv, exceptdaylist=[]):
        self.__subjectlist = CSVUtil(subjectcsv).getClassList(Subject)
        self.__exceptdaylist = exceptdaylist
        self.__hasexamterm = False

    # 特別時間割登録
    def setExceptDayFromFile(self, filename):
        self.__exceptdaylist = CSVUtil(filename).getClassList(ExceptDay)

    # 考査期間を設定する
    # 無効にする場合はbool一つで良いが、有効にする場合はbool,starttime,endtimeが必要
    def setExamTeam(self, hasexamterm, startdate=None, enddate=None):
        self.__hasexamterm = hasexamterm
        if hasexamterm:
            if startdate == None or enddate == None:
                raise NoExamTimeException
            self.__startdate = startdate
            self.__enddate = enddate
            # 二行目から考査情報取得
            self.__examtimelist = self.__subjectlist[0].getTimeList()

    # 教員IDからSubjectクラスを取得
    def getSubjectFromTeacherId(self, teacherid, now, minute=60):
        subjectlist = []
        examlist = []
        detectsubject = []
        # 今日のExceptListを取得
        exceptlist = self.getTodayExceptList(now)
        
        # 考査期間かそうでないかのフラグ生成
        if self.__hasexamterm and self.__startdate.date() <= now.date() and self.__enddate.date() >= now.date():
            isexamterm = True
        else:
            isexamterm = False
        # 特別時間割が存在するかのフラグ生成
        if len(exceptlist) != 0:
            isexceptday = True
        else:
            isexceptday = False

        # 該当Subjectクラスリスト生成
        for subject in self.__subjectlist[1:]:
            if subject.isThisSubjectTeacher(teacherid):
                subjectlist.append(subject)
                if subject.hasExam() and isexamterm:
                    examlist.append(self.makeExamSubject(subject))

        # 該当から絞り込み
        for subject in subjectlist:
            isexceptsubject = False
            # 講義が特別時間割に該当する場合、時間を変更し次の判定に回す
            if isexceptday:
                for exceptday in exceptlist:
                    if exceptday.isThisSubjectId(subject.getSubjectId()):
                        starttime, endtime = exceptday.getTime()
                        subject.setTime(starttime, endtime)
                        isexceptsubject = True
            # 特別時間割か曜日が等しい、かつ時間が近い場合該当講義だと判別
            if (isexceptsubject or subject.isThisSubjectWeekDay(now.weekday())) and subject.isNearTime(now.time(), minute):
                if isexamterm and subject.hasExam():
                    detectsubject.append(self.makeExamSubject(subject)) # 試験期間かつ試験が存在する講義
                else:
                    detectsubject.append(subject) # 通常講義

        return detectsubject, subjectlist, examlist

    # 今日の特別時間割のリスト取得
    def getTodayExceptList(self, now):
        exceptlist = []
        for exceptday in self.__exceptdaylist:
            if exceptday.isExceptDay(now):
                exceptlist.append(exceptday)

        return exceptlist
        
    # 考査用の講義を作成する
    def makeExamSubject(self, subject):
        exam = copy.deepcopy(subject)
        exam.setExam(True)
        exam.setBorder(self.__examtimelist[1], self.__examtimelist[2])

        return exam

    # 講義IDからSubjectクラスを取得
    def getSubjectFromSubjectId(self, subjectid):
        for subject in self.__subjectlist:
            if subject.isThisSubjectId(subjectid):
                return subject
        return None
