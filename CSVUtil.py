import csv

class CSVUtil:
    def __init__(self, filename):
        self.header, self.data = self.read(filename, True)

    def printHeader(self):
        print(self.header)

    # クラスのリストを作成
    def getClassList(self, csvclass):
        classlist = []
        for list in self.data:
            classlist.append(csvclass(list))
        return classlist

    @classmethod
    def read(cls, filename, gethdr=False):
        data = []
        with open(filename, "r", encoding="utf-8") as file:
            reader = csv.reader(file)

            header = next(reader) # 最初の行はヘッダとして読む
            for row in reader:
                data.append(row)
            if(gethdr):
                return header, data
            else:
                return data

    @classmethod
    def readHeader(cls, filename):
        with open(filename, "r", encoding="utf-8") as file:
            reader = csv.reader(file)

            return next(reader)

    @classmethod
    def write(cls, data, filename):
        with open(filename, "w", encoding="utf-8", newline="") as outfile:
            writer = csv.writer(outfile)

            writer.writerows(data)

