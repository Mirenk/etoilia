import datetime

#
# 生徒クラス(基底)
#
class Student:
    def __init__(self, idnum, name, ruby, sex):
        self.idnum = idnum
        self.name = name
        self.ruby = ruby
        self.sex = sex

    def getIdNumber(self):
        return self.idnum
        
    def getName(self):
        return self.name
        
    def getRuby(self):
        return self.ruby

    def getSex(self):
        return self.sex

    def getList(self):
        return [self.idnum, self.name, self.ruby, self.sex]

#
# 履修者クラス
#
class StudentCSV(Student):
    def __init__(self, list):
        super().__init__(list[0], list[1], list[2], list[3])
        self.__idm = list[4]

    def getIdm(self):
        return self.__idm

    def isMyIdm(self, idm):
        if(idm == self.__idm):
            return True
        return False

#
# 生徒の出席クラス
#
class StudentAttendance(Student):
    def __init__(self, idnum, name, ruby, sex, timelist):
        super().__init__(idnum, name, ruby, sex)
        self.__timelist = timelist
        self.__attendtime = None
        self.__attendance = "×"

    # 各時間より出席状況を判定
    def setAttendance(self, attendtime):
        # 先に記録されている(欠席以外)の場合、更新せず処理を中断する。
        if self.__attendance != "×":
            print("既に出席記録されています。")
            return

        # 出席時間セット
        self.__attendtime = attendtime
        attendtime = datetime.datetime.strptime(attendtime, "%H:%M:%S")
        
        # 各規則時間をセット
        self.__starttime = self.__timelist[0]
        self.__attendborder = int(self.__timelist[1])
        self.__lateborder = int(self.__timelist[2])

        starttime = datetime.datetime.strptime(self.__starttime, "%H:%M")
        attendlimit = starttime + datetime.timedelta(minutes=self.__attendborder)
        latelimit = starttime + datetime.timedelta(minutes=self.__lateborder)

        # 出欠判定
        # ここでメッセージも出力する
        if attendtime < attendlimit:
            print("出席です")
            self.__attendance = "〇"
        elif attendtime < latelimit:
            print("遅刻です")
            self.__attendance = "△"
        else:
            print("欠席です")

    # StudentクラスとSubjectクラスから生成(初回CSV書き出し時等に使用)
    @classmethod
    def fromStudent(cls, student, timelist):
        idnum = student.getIdNumber()
        name = student.getName()
        ruby = student.getRuby()
        sex = student.getSex()
        return cls(idnum, name, ruby, sex, timelist)

    # List型から生成(遅刻時間規則変更時等に使用)
    @classmethod
    def fromList(cls, list, timelist):
        instance = cls(list[0], list[1], list[2], list[3], timelist)
        instance.setAttendance(list[4])
        return instance

    # クラスをリスト化(CSV書き出し時等使用)
    # 第二引数で各規則時間を記録するか指定, 第三引数で規則時間分の要素を入れるか(パディング)指定
    def getList(self, gettimelist=False, padding=False):
        tmplist = [self.idnum, self.name, self.ruby, self.sex, self.__attendtime, self.__attendance]
        if gettimelist:
            return tmplist + self.__timelist
        elif padding:
            return tmplist + [ "", "", "", ""]
        else:
            return tmplist