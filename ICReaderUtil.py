from CSVUtil import CSVUtil
from abc import ABC
from abc import abstractmethod
from collections import deque

#
# 読み込んだデータのクラス(構造体)
#
class ReadData:
    def __init__(self, date, time, idm):
        self.__date = date
        self.__time = time
        self.__idm = idm

    @classmethod
    def fromCSV(cls, list):
        return cls(list[0], list[1], list[2])

    def getDate(self):
        return self.__date

    def getTime(self):
        return self.__time

    def getIdm(self):
        return self.__idm

#
# ICリーダ例外
#
# 物理ICリーダでは時間経過で、疑似カードリーダではリスト終わりでこれを吐く
class ICReaderInterrupt(Exception):
    pass

#
# ICカードリーダー抽象基底クラス
#
class AbstractICReader(ABC):
    # ReadDataクラスで返す
    @abstractmethod
    def getReadData(self):
        pass

    @abstractmethod
    def getDate(self):
        pass

#
# 疑似ICリーダークラス
#
class FakeICReader(AbstractICReader):
    def __init__(self, filename):
        self.__filename = filename
        
        datalist =  CSVUtil(self.__filename).getClassList(ReadData.fromCSV)
        self.__date = datalist[0].getDate()
        self.__dataqueue = deque(datalist)

    # リストから疑似的にデータを返す
    def getReadData(self):
        try:
            return self.__dataqueue.popleft()
        except IndexError:
            raise ICReaderInterrupt

    def getDate(self):
        return self.__date

#
# ICリーダークラス(未実装)
# 
class RealICReader(AbstractICReader):
    def __init__(self):
        return

    def getReadData(self):
        return
    
    def getDate(self):
        return